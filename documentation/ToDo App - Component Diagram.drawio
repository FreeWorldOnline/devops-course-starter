<mxfile host="app.diagrams.net" modified="2023-01-03T14:25:06.827Z" agent="5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36" etag="fqhKTKp2fZjuDwU3M4M6" version="20.8.0" type="gitlab">
  <diagram id="nby9-P7HMrFqxZ-erkNj" name="Page-1">
    <mxGraphModel dx="1434" dy="764" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="827" math="0" shadow="0">
      <root>
        <mxCell id="0" />
        <mxCell id="1" parent="0" />
        <object placeholders="1" c4Name="ToDo App User" c4Type="Person" c4Description="A user of the app with tasks to organise" label="&lt;font style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;%c4Name%&lt;/b&gt;&lt;/font&gt;&lt;div&gt;[%c4Type%]&lt;/div&gt;&lt;br&gt;&lt;div&gt;&lt;font style=&quot;font-size: 11px&quot;&gt;&lt;font color=&quot;#cccccc&quot;&gt;%c4Description%&lt;/font&gt;&lt;/div&gt;" id="TfVIJM7xZkjsP2LaU7dm-2">
          <mxCell style="html=1;fontSize=11;dashed=0;whiteSpace=wrap;fillColor=#083F75;strokeColor=#06315C;fontColor=#ffffff;shape=mxgraph.c4.person2;align=center;metaEdit=1;points=[[0.5,0,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0]];resizable=0;" parent="1" vertex="1">
            <mxGeometry x="40" y="40" width="200" height="180" as="geometry" />
          </mxCell>
        </object>
        <object placeholders="1" c4Name="Trello" c4Type="Software System" c4Description="Stores all tasks and their current status" label="&lt;font style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;%c4Name%&lt;/b&gt;&lt;/font&gt;&lt;div&gt;[%c4Type%]&lt;/div&gt;&lt;br&gt;&lt;div&gt;&lt;font style=&quot;font-size: 11px&quot;&gt;&lt;font color=&quot;#cccccc&quot;&gt;%c4Description%&lt;/font&gt;&lt;/div&gt;" id="TfVIJM7xZkjsP2LaU7dm-4">
          <mxCell style="rounded=1;whiteSpace=wrap;html=1;labelBackgroundColor=none;fillColor=#8C8496;fontColor=#ffffff;align=center;arcSize=10;strokeColor=#736782;metaEdit=1;resizable=0;points=[[0.25,0,0],[0.5,0,0],[0.75,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]];" parent="1" vertex="1">
            <mxGeometry x="880" y="640" width="240" height="120" as="geometry" />
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Trello Items" c4Container="Container" c4Technology="Python Class" c4Description="Interface style class for providing access to underlying Trello storage mechanism" label="&lt;font style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;%c4Type%&lt;/b&gt;&lt;/font&gt;&lt;div&gt;[%c4Container%:&amp;nbsp;%c4Technology%]&lt;/div&gt;&lt;br&gt;&lt;div&gt;&lt;font style=&quot;font-size: 11px&quot;&gt;&lt;font color=&quot;#E6E6E6&quot;&gt;%c4Description%&lt;/font&gt;&lt;/div&gt;" id="CgbnRv5LnPjGI7KwlvaE-3">
          <mxCell style="shape=cylinder3;size=15;direction=south;whiteSpace=wrap;html=1;boundedLbl=1;rounded=0;labelBackgroundColor=none;fillColor=#23A2D9;fontSize=12;fontColor=#ffffff;align=center;strokeColor=#0E7DAD;metaEdit=1;points=[[0.5,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.5,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]];resizable=0;" parent="1" vertex="1">
            <mxGeometry x="880" y="414" width="240" height="120" as="geometry" />
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Relationship" c4Technology="JSON/REST" c4Description="Makes API calls using" label="&lt;div style=&quot;text-align: left&quot;&gt;&lt;div style=&quot;text-align: center&quot;&gt;&lt;b&gt;%c4Description%&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center&quot;&gt;[%c4Technology%]&lt;/div&gt;&lt;/div&gt;" id="CgbnRv5LnPjGI7KwlvaE-9">
          <mxCell style="endArrow=blockThin;html=1;fontSize=10;fontColor=#404040;strokeWidth=1;endFill=1;strokeColor=#828282;elbow=vertical;metaEdit=1;endSize=14;startSize=14;jumpStyle=arc;jumpSize=16;rounded=0;edgeStyle=orthogonalEdgeStyle;exitX=1;exitY=0.5;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0.5;entryY=0;entryDx=0;entryDy=0;entryPerimeter=0;dashed=1;" parent="1" source="CgbnRv5LnPjGI7KwlvaE-3" target="TfVIJM7xZkjsP2LaU7dm-4" edge="1">
            <mxGeometry width="240" relative="1" as="geometry">
              <mxPoint x="700" y="640" as="sourcePoint" />
              <mxPoint x="940" y="640" as="targetPoint" />
            </mxGeometry>
          </mxCell>
        </object>
        <object placeholders="1" c4Name="[Components] Diagram for ToDo App - Webserver" c4Type="ContainerDiagramTitle" c4Description="This is a Components diagram for the ToDo app" label="&lt;font style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;&lt;div style=&quot;text-align: left&quot;&gt;%c4Name%&lt;/div&gt;&lt;/b&gt;&lt;/font&gt;&lt;div style=&quot;text-align: left&quot;&gt;%c4Description%&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-1">
          <mxCell style="text;html=1;strokeColor=none;fillColor=none;align=left;verticalAlign=top;whiteSpace=wrap;rounded=0;metaEdit=1;allowArrows=0;resizable=1;rotatable=0;connectable=0;recursiveResize=0;expand=0;pointerEvents=0;points=[[0.25,0,0],[0.5,0,0],[0.75,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]];" parent="1" vertex="1">
            <mxGeometry x="395" y="20" width="380" height="40" as="geometry" />
          </mxCell>
        </object>
        <object placeholders="1" c4Name="Webserver" c4Type="ContainerScopeBoundary" c4Application="Container" label="&lt;font style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;&lt;div style=&quot;text-align: left&quot;&gt;%c4Name%&lt;/div&gt;&lt;/b&gt;&lt;/font&gt;&lt;div style=&quot;text-align: left&quot;&gt;[%c4Application%]&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-3">
          <mxCell style="rounded=1;fontSize=11;whiteSpace=wrap;html=1;dashed=1;arcSize=20;fillColor=none;strokeColor=#666666;fontColor=#333333;labelBackgroundColor=none;align=left;verticalAlign=bottom;labelBorderColor=none;spacingTop=0;spacing=10;dashPattern=8 4;metaEdit=1;rotatable=0;perimeter=rectanglePerimeter;noLabel=0;labelPadding=0;allowArrows=0;connectable=0;expand=0;recursiveResize=0;editable=1;pointerEvents=0;absoluteArcSize=1;points=[[0.25,0,0],[0.5,0,0],[0.75,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]];" parent="1" vertex="1">
            <mxGeometry x="40" y="230" width="760" height="530" as="geometry" />
          </mxCell>
        </object>
        <object placeholders="1" c4Name="Index Controller" c4Type="Component" c4Technology="Flask Route" c4Description="Serves the web application to the end user with current task data" label="&lt;font style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;%c4Name%&lt;/b&gt;&lt;/font&gt;&lt;div&gt;[%c4Type%: %c4Technology%]&lt;/div&gt;&lt;br&gt;&lt;div&gt;&lt;font style=&quot;font-size: 11px&quot;&gt;%c4Description%&lt;/font&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-4">
          <mxCell style="rounded=1;whiteSpace=wrap;html=1;labelBackgroundColor=none;fillColor=#63BEF2;fontColor=#ffffff;align=center;arcSize=6;strokeColor=#2086C9;metaEdit=1;resizable=0;points=[[0.25,0,0],[0.5,0,0],[0.75,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]];" parent="1" vertex="1">
            <mxGeometry x="440" y="240" width="240" height="120" as="geometry" />
          </mxCell>
        </object>
        <object placeholders="1" c4Name="New Item Controller" c4Type="Component" c4Technology="Flask Route" c4Description="Provides an asynchronous mechanism for adding new tasks" label="&lt;font style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;%c4Name%&lt;/b&gt;&lt;/font&gt;&lt;div&gt;[%c4Type%: %c4Technology%]&lt;/div&gt;&lt;br&gt;&lt;div&gt;&lt;font style=&quot;font-size: 11px&quot;&gt;%c4Description%&lt;/font&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-5">
          <mxCell style="rounded=1;whiteSpace=wrap;html=1;labelBackgroundColor=none;fillColor=#63BEF2;fontColor=#ffffff;align=center;arcSize=6;strokeColor=#2086C9;metaEdit=1;resizable=0;points=[[0.25,0,0],[0.5,0,0],[0.75,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]];" parent="1" vertex="1">
            <mxGeometry x="440" y="370" width="240" height="120" as="geometry" />
          </mxCell>
        </object>
        <object placeholders="1" c4Name="Update Item Controller" c4Type="Component" c4Technology="Flask Route" c4Description="Provides an asynchronous mechanism for updating existing tasks" label="&lt;font style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;%c4Name%&lt;/b&gt;&lt;/font&gt;&lt;div&gt;[%c4Type%: %c4Technology%]&lt;/div&gt;&lt;br&gt;&lt;div&gt;&lt;font style=&quot;font-size: 11px&quot;&gt;%c4Description%&lt;/font&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-6">
          <mxCell style="rounded=1;whiteSpace=wrap;html=1;labelBackgroundColor=none;fillColor=#63BEF2;fontColor=#ffffff;align=center;arcSize=6;strokeColor=#2086C9;metaEdit=1;resizable=0;points=[[0.25,0,0],[0.5,0,0],[0.75,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]];" parent="1" vertex="1">
            <mxGeometry x="440" y="500" width="240" height="120" as="geometry" />
          </mxCell>
        </object>
        <object placeholders="1" c4Name="Delete Item Controller" c4Type="Component" c4Technology="Flask Route" c4Description="Provides an asynchronous mechanism for deleting existing tasks" label="&lt;font style=&quot;font-size: 16px&quot;&gt;&lt;b&gt;%c4Name%&lt;/b&gt;&lt;/font&gt;&lt;div&gt;[%c4Type%: %c4Technology%]&lt;/div&gt;&lt;br&gt;&lt;div&gt;&lt;font style=&quot;font-size: 11px&quot;&gt;%c4Description%&lt;/font&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-7">
          <mxCell style="rounded=1;whiteSpace=wrap;html=1;labelBackgroundColor=none;fillColor=#63BEF2;fontColor=#ffffff;align=center;arcSize=6;strokeColor=#2086C9;metaEdit=1;resizable=0;points=[[0.25,0,0],[0.5,0,0],[0.75,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]];" parent="1" vertex="1">
            <mxGeometry x="440" y="630" width="240" height="120" as="geometry" />
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Relationship" c4Technology="HTML/HTTP" c4Description="Visits ToDo App webpage using" label="&lt;div style=&quot;text-align: left&quot;&gt;&lt;div style=&quot;text-align: center&quot;&gt;&lt;b&gt;%c4Description%&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center&quot;&gt;[%c4Technology%]&lt;/div&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-9">
          <mxCell style="endArrow=blockThin;html=1;fontSize=10;fontColor=#404040;strokeWidth=1;endFill=1;strokeColor=#828282;elbow=vertical;metaEdit=1;endSize=14;startSize=14;jumpStyle=arc;jumpSize=16;rounded=0;edgeStyle=orthogonalEdgeStyle;exitX=1;exitY=0.75;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0;entryY=0.5;entryDx=0;entryDy=0;entryPerimeter=0;" parent="1" source="TfVIJM7xZkjsP2LaU7dm-2" target="Au4q5l7vwmWdxRWBAT2p-4" edge="1">
            <mxGeometry x="0.1385" width="240" relative="1" as="geometry">
              <mxPoint x="290" y="150" as="sourcePoint" />
              <mxPoint x="530" y="150" as="targetPoint" />
              <mxPoint as="offset" />
            </mxGeometry>
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Relationship" c4Technology="AJAX/HTTP" c4Description="Adds a new task using" label="&lt;div style=&quot;text-align: left&quot;&gt;&lt;div style=&quot;text-align: center&quot;&gt;&lt;b&gt;%c4Description%&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center&quot;&gt;[%c4Technology%]&lt;/div&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-10">
          <mxCell style="endArrow=blockThin;html=1;fontSize=10;fontColor=#404040;strokeWidth=1;endFill=1;strokeColor=#828282;elbow=vertical;metaEdit=1;endSize=14;startSize=14;jumpStyle=arc;jumpSize=16;rounded=0;edgeStyle=orthogonalEdgeStyle;exitX=0.75;exitY=1;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0;entryY=0.5;entryDx=0;entryDy=0;entryPerimeter=0;dashed=1;" parent="1" source="TfVIJM7xZkjsP2LaU7dm-2" target="Au4q5l7vwmWdxRWBAT2p-5" edge="1">
            <mxGeometry width="240" relative="1" as="geometry">
              <mxPoint x="220" y="380" as="sourcePoint" />
              <mxPoint x="460" y="380" as="targetPoint" />
            </mxGeometry>
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Relationship" c4Technology="AJAX/HTTP" c4Description="Edits and existing task using" label="&lt;div style=&quot;text-align: left&quot;&gt;&lt;div style=&quot;text-align: center&quot;&gt;&lt;b&gt;%c4Description%&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center&quot;&gt;[%c4Technology%]&lt;/div&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-11">
          <mxCell style="endArrow=blockThin;html=1;fontSize=10;fontColor=#404040;strokeWidth=1;endFill=1;strokeColor=#828282;elbow=vertical;metaEdit=1;endSize=14;startSize=14;jumpStyle=arc;jumpSize=16;rounded=0;edgeStyle=orthogonalEdgeStyle;exitX=0.5;exitY=1;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0;entryY=0.5;entryDx=0;entryDy=0;entryPerimeter=0;dashed=1;" parent="1" source="TfVIJM7xZkjsP2LaU7dm-2" target="Au4q5l7vwmWdxRWBAT2p-6" edge="1">
            <mxGeometry x="0.1875" y="30" width="240" relative="1" as="geometry">
              <mxPoint x="260" y="550" as="sourcePoint" />
              <mxPoint x="500" y="550" as="targetPoint" />
              <mxPoint as="offset" />
            </mxGeometry>
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Relationship" c4Technology="AJAX/HTTP" c4Description="Deletes an item using" label="&lt;div style=&quot;text-align: left&quot;&gt;&lt;div style=&quot;text-align: center&quot;&gt;&lt;b&gt;%c4Description%&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center&quot;&gt;[%c4Technology%]&lt;/div&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-12">
          <mxCell style="endArrow=blockThin;html=1;fontSize=10;fontColor=#404040;strokeWidth=1;endFill=1;strokeColor=#828282;elbow=vertical;metaEdit=1;endSize=14;startSize=14;jumpStyle=arc;jumpSize=16;rounded=0;edgeStyle=orthogonalEdgeStyle;exitX=0.25;exitY=1;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0;entryY=0.5;entryDx=0;entryDy=0;entryPerimeter=0;dashed=1;" parent="1" source="TfVIJM7xZkjsP2LaU7dm-2" target="Au4q5l7vwmWdxRWBAT2p-7" edge="1">
            <mxGeometry y="10" width="240" relative="1" as="geometry">
              <mxPoint x="210" y="660" as="sourcePoint" />
              <mxPoint x="450" y="660" as="targetPoint" />
              <mxPoint as="offset" />
            </mxGeometry>
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Relationship" c4Technology="Python" c4Description="Call relevant CRUD method" label="&lt;div style=&quot;text-align: left&quot;&gt;&lt;div style=&quot;text-align: center&quot;&gt;&lt;b&gt;%c4Description%&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center&quot;&gt;[%c4Technology%]&lt;/div&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-13">
          <mxCell style="endArrow=blockThin;html=1;fontSize=10;fontColor=#404040;strokeWidth=1;endFill=1;strokeColor=#828282;elbow=vertical;metaEdit=1;endSize=14;startSize=14;jumpStyle=arc;jumpSize=16;rounded=0;edgeStyle=orthogonalEdgeStyle;exitX=1;exitY=0.5;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0.5;entryY=1;entryDx=0;entryDy=0;entryPerimeter=0;" parent="1" source="Au4q5l7vwmWdxRWBAT2p-5" target="CgbnRv5LnPjGI7KwlvaE-3" edge="1">
            <mxGeometry x="0.1803" width="240" relative="1" as="geometry">
              <mxPoint x="840" y="230" as="sourcePoint" />
              <mxPoint x="1080" y="230" as="targetPoint" />
              <mxPoint as="offset" />
            </mxGeometry>
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Relationship" c4Technology="JSON/REST" c4Description="Makes API calls using" label="&lt;div style=&quot;text-align: left; font-size: 1px;&quot;&gt;&lt;div style=&quot;text-align: center; font-size: 1px;&quot;&gt;&lt;b style=&quot;font-size: 1px;&quot;&gt;%c4Description%&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center; font-size: 1px;&quot;&gt;[%c4Technology%]&lt;/div&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-20">
          <mxCell style="endArrow=blockThin;html=1;fontSize=1;fontColor=#404040;strokeWidth=1;endFill=1;strokeColor=#828282;elbow=vertical;metaEdit=1;endSize=14;startSize=14;jumpStyle=arc;jumpSize=16;rounded=0;edgeStyle=orthogonalEdgeStyle;exitX=1.008;exitY=0.558;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0.5;entryY=1;entryDx=0;entryDy=0;entryPerimeter=0;" parent="1" source="Au4q5l7vwmWdxRWBAT2p-4" target="CgbnRv5LnPjGI7KwlvaE-3" edge="1">
            <mxGeometry width="240" relative="1" as="geometry">
              <mxPoint x="700" y="300" as="sourcePoint" />
              <mxPoint x="900" y="344" as="targetPoint" />
            </mxGeometry>
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Relationship" c4Technology="JSON/REST" c4Description="Makes API calls using" label="&lt;div style=&quot;text-align: left; font-size: 1px;&quot;&gt;&lt;div style=&quot;text-align: center; font-size: 1px;&quot;&gt;&lt;b style=&quot;font-size: 1px;&quot;&gt;%c4Description%&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center; font-size: 1px;&quot;&gt;[%c4Technology%]&lt;/div&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-22">
          <mxCell style="endArrow=blockThin;html=1;fontSize=1;fontColor=#404040;strokeWidth=1;endFill=1;strokeColor=#828282;elbow=vertical;metaEdit=1;endSize=14;startSize=14;jumpStyle=arc;jumpSize=16;rounded=0;edgeStyle=orthogonalEdgeStyle;exitX=1;exitY=0.5;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0.5;entryY=1;entryDx=0;entryDy=0;entryPerimeter=0;" parent="1" source="Au4q5l7vwmWdxRWBAT2p-6" target="CgbnRv5LnPjGI7KwlvaE-3" edge="1">
            <mxGeometry width="240" relative="1" as="geometry">
              <mxPoint x="691.9200000000001" y="316.96000000000004" as="sourcePoint" />
              <mxPoint x="890" y="484" as="targetPoint" />
            </mxGeometry>
          </mxCell>
        </object>
        <object placeholders="1" c4Type="Relationship" c4Technology="JSON/REST" c4Description="Makes API calls using" label="&lt;div style=&quot;text-align: left; font-size: 1px;&quot;&gt;&lt;div style=&quot;text-align: center; font-size: 1px;&quot;&gt;&lt;b style=&quot;font-size: 1px;&quot;&gt;%c4Description%&lt;/b&gt;&lt;/div&gt;&lt;div style=&quot;text-align: center; font-size: 1px;&quot;&gt;[%c4Technology%]&lt;/div&gt;&lt;/div&gt;" id="Au4q5l7vwmWdxRWBAT2p-23">
          <mxCell style="endArrow=blockThin;html=1;fontSize=1;fontColor=#404040;strokeWidth=1;endFill=1;strokeColor=#828282;elbow=vertical;metaEdit=1;endSize=14;startSize=14;jumpStyle=arc;jumpSize=16;rounded=0;edgeStyle=orthogonalEdgeStyle;exitX=1;exitY=0.5;exitDx=0;exitDy=0;exitPerimeter=0;entryX=0.5;entryY=1;entryDx=0;entryDy=0;entryPerimeter=0;" parent="1" source="Au4q5l7vwmWdxRWBAT2p-7" target="CgbnRv5LnPjGI7KwlvaE-3" edge="1">
            <mxGeometry width="240" relative="1" as="geometry">
              <mxPoint x="690" y="570" as="sourcePoint" />
              <mxPoint x="890" y="484" as="targetPoint" />
            </mxGeometry>
          </mxCell>
        </object>
      </root>
    </mxGraphModel>
  </diagram>
</mxfile>
