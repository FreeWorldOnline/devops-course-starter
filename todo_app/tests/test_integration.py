import os
import pytest
import requests
from todo_app import app
from dotenv import load_dotenv, find_dotenv
import todo_app.data.mongo_items
import mongomock

@pytest.fixture
def client():
    # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)

    # Create the new app.
    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        test_app = app.create_app()

        # Use the app to create a test_client that can be used in our tests.
        with test_app.test_client() as client:
            yield client

@pytest.fixture
def test_card_id():
    #Add a Test card directly to Mongo for basic tests
    insert_item = todo_app.data.mongo_items.get_mongo_collection().insert_one({"name": "Test card", "status": "To Do"})
    return str(insert_item.inserted_id)

def test_add_item(client):
    # Make a request to the add item route
    response = client.post('/add_item', data=dict(
        new_title='Test new item',
    ))

    assert response.status_code == 302

def test_index_page(client, test_card_id):
    # Make a request to our app's index page
    response = client.get('/')

    assert response.status_code == 200
    resp_data = response.data.decode()
    assert 'itemCard_' + test_card_id in resp_data
    assert 'Test card' in resp_data

def test_save_item(client, test_card_id):
    # Make a request to the update item route
    response = client.put('/update_item', data=dict(
        item_id=test_card_id,
        lane='done'
    ))

    assert response.status_code == 200

def test_delete_item(client, test_card_id):
    # Make a request to the delete item route
    response = client.delete('/delete_item', data=dict(
        item_id=test_card_id
    ))

    assert response.status_code == 200