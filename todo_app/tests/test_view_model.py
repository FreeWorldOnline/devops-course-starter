import pytest
from todo_app.data.item import Item
from todo_app.templates.view_model import ViewModel

@pytest.fixture
def view_model() -> ViewModel:
    items = []
    count = 1

    #Create to-do items
    for x in range(0, 5):
        items.append(Item("test" + str(count), "Test Item " + str(count)))
        count += 1
    #Create doing items
    for x in range(0, 3):
        items.append(Item("test" + str(count), "Test Item " + str(count), "Doing"))
        count += 1
    #Create done items
    for x in range(0, 7):
        items.append(Item("test" + str(count), "Test Item " + str(count), "Done"))
        count += 1

    model = ViewModel(items)
    return model

def test_to_do_items(view_model: ViewModel):
    assert view_model.to_do_items.__len__() == 5

def test_doing_items(view_model: ViewModel):
    assert view_model.doing_items.__len__() == 3

def test_done_items(view_model: ViewModel):
    assert view_model.done_items.__len__() == 7