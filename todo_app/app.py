from flask import Flask, redirect, render_template, request

from todo_app.flask_config import Config
from todo_app.data import mongo_items
from todo_app.templates.view_model import ViewModel

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())

    #List of current to-do items
    @app.route('/')
    def index():
        item_view_model = ViewModel(mongo_items.get_items())
        return render_template('index.html', view_model=item_view_model)

    #New to-do items
    @app.route('/add_item', methods=['POST'])
    def new_item():
        mongo_items.add_item(request.form.get('new_title'))
        return redirect('/')

    #Update existing item (called asynchronusly)
    @app.route('/update_item', methods=['PUT'])
    def update_item():
        item_id = request.form.get('item_id')
        lane = request.form.get('lane')
        item = mongo_items.get_item(item_id)
        if (item == None):
            return 'Unable to find an item with ID ' + item_id
        
        if lane == 'done':
            item.status = 'Done'
        elif lane == 'doing':
            item.status = 'Doing'
        else:
            item.status = 'To Do'
        mongo_items.save_item(item)

        return ''

    #Delete an item (called asynchronusly)
    @app.route('/delete_item', methods=['DELETE'])
    def delete_item():
        item_id = request.form.get('item_id')
        item = mongo_items.get_item(item_id)
        if (item == None):
            return 'Unable to find an item with ID ' + item_id
        
        mongo_items.delete_item(item)

        return ''
    
    return app