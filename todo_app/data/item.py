class Item:
    def __init__(self, id, title, status = "To Do") -> None:
        self.id = id
        self.title = title
        self.status = status
    
    @classmethod
    def from_mongo_item(cls, item):
        return cls(item["_id"], item["name"], item["status"])
    
    @classmethod
    def get_items_from_mongo_collecton(cls, json_object):
        result = []
        for item in json_object:
            result.append(Item.from_mongo_item(item))
        return result