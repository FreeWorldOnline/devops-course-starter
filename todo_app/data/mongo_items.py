import os
import pymongo
from bson.objectid import ObjectId

from todo_app.data.item import Item

def get_mongo_collection():
    """
    Initalize the Mongo integration and return a instance of the collection containing the user's task data

    Returns: Mongo collection instance
    """
    client = pymongo.MongoClient(os.getenv("MONGO_CONN_STRING"))
    db = client[os.getenv("MONGO_DATABASE")]
    return db[os.getenv("MONGO_COLLECTION")]

def get_items():
    """
    Fetches all saved items from Mongo.

    Returns:
        list: The list of saved items.
    """
    tasks = get_mongo_collection()

    return Item.get_items_from_mongo_collecton(tasks.find())


def get_item(id):
    """
    Fetches the saved item with the specified ID.

    Args:
        id: The ID of the item.

    Returns:
        item: The saved item, or None if no items match the specified ID.
    """
    tasks = get_mongo_collection()
    exist_item = tasks.find_one({"_id": ObjectId(id)})

    if exist_item != None:
        return Item.from_mongo_item(exist_item)
    else:
        return None


def add_item(title):
    """
    Adds a new item with the specified title to Mongo.

    Args:
        title: The title of the item.

    Returns:
        item: The saved item.
    """
    tasks = get_mongo_collection()
    insert_item = tasks.insert_one({"name": title, "status": "To Do"})

    return Item(insert_item.inserted_id, title)

def save_item(item):
    """
    Updates an existing item in Mongo. If no existing item matches the ID of the specified item, nothing is saved.

    Args:
        item: The item to save.
    """
    tasks = get_mongo_collection()
    update_item = tasks.update_one( {"_id": ObjectId(item.id)},
                                    {"$set": {"name": item.title, "status": item.status} })
    
    return item

def delete_item(item):
    """
    Deletes an existing item from Mongo. If no existing item matches the ID of the specified item, nothing is deleted.

    Args:
        item: The item to delete.
    """
    tasks = get_mongo_collection()
    delete_item = tasks.delete_one({"_id": ObjectId(item.id)})
    
    return None