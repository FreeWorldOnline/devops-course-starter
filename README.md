# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## MongoDB

The project also makes use of MongoDB to store the user's list of to-do items along with their current status
Prior to running the app: create a new CosmosDB on your [Azure Portal](https://portal.azure.com):
1. New -> CosmosDB Database
2. Find and select the "Azure Cosmos DB API for MongoDB"
3. Set a name for the database and choose "Serverless" capacity mode, leave other settings at defaults
4. Optionally create a collection, however this should be created automatically if it is missing during the first run
5. Navigate to "Connection strings" and note down the "PRIMARY CONNECTION STRING"

Update your `.env` file with the Mongo Connection String, Database and Collection names

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Testing

Automated testing has been written and is using the pytest framework.
Dev dependency for pytest has been added to poetry using the --dev switch

Test can be run using the poetry run pytest command from the console
Alternatively if using VSCode you can use the inbuilt Testing module (see the lab glass icon), ensure you pick pytest as the testing tool

## Ansible
Host / managed nodes can be stood up using Ansible from a control node that has access to them (i.e. ssh key generated using ssh-keygen on the control node and copied to all managed nodes using ssh-copy-id user@hostname-or-ip)

Inventory, playbook and environment varible template should be copied from the /ansible directory of this repository into the working directory of the control node

Afterwards; from the control node execute the following to deploy and start the ToDoApp on all managed nodes:
ansible-playbook playbook -i inventory

Note you will be prompted for the flask environment, secret key, mongo connection string, mongo database and mongo collection

Once deployed you should be able to access the ToDoApp in your browser; http://hostname-or-ip:5000/

## Docker
The application can be run from within a Docker Container, use the following commands to build the todo_app images for the various environments:
docker build --tag todo-app .
docker build --target development --tag todo-app:dev .
docker build --target test --tag todo-app:test .
docker build --target production --tag todo-app:latest .

To run the image for Dev (using Flask in debug mode) use the below command:
docker run --mount type=volume,source=todo_app,destination=/todo_app -p 80:5000 --env-file .env todo-app:ENV

To run the image in Test mode (executes the automated tests using pytest) use the below command:
docker run --env-file .env.test todo-app:test

To run the image for Prod (using Gunicorn) use the below command:
docker run --detach -p 80:8000 --env-file .env todo-app:latest

Afterwards you should be able to access the ToDoApp in your browser on your host machine; http://localhost/
(Note: we bound the container to default port 80 on the host hence no port needed in the URL)


## Azure Web App Deployment
Follow below steps after building the Prod (latest) docker image to publish the application via an Azure Web App.
An example of a working Azure Web App can be found by visiting https://davbla-todo.azurewebsites.net/
It is using the latest image from https://hub.docker.com/repository/docker/dblasdell/todo-app/general

The instructions assume you have a Azure Account (for https://portal.azure.com) and have created a Resource Group to contain your application.
Note that <webapp_name> has to be globally unique as it will form the public URL for the application (e.g. https://<webapp_name>.azurewebsites.net)
Repeat the final step for each environment key value pair from your .env file

Once complete test your application by visiting http://<webapp_name>.azurewebsites.net/

docker login
docker push <docker_user_name>/todo-app:latest

az login
az appservice plan create --resource-group <resource_group_name> -n <appservice_plan_name> --sku B1 --is-linux
az webapp create --resource-group <resource_group_name> --plan <appservice_plan_name> --name <webapp_name> --deployment-container-image-name docker.io/<docker_user_name>/todo-app:latest

az webapp config appsettings set -g <resource_group_name> -n <webapp_name> --settings <ENV_KEY=env_value>


## Automate DockerHub Publish and Azure Deployment
The GitLab CI/CD Pipeline generated by gitlab-ci.yml will:
1) Publish a successful build to DockerHub
2) Tell the Azure Web App (setup based on previous instructions) to re-deploy following a succesfull build & publish

To configure this correcly add CI/CD Variables for the following:
DOCKER_REGISTRY - e.g. docker.io
DOCKER_REPO - <docker_user_name>/todo-app
DOCKER_USER
DOCKER_PASS
AZURE_WEBHOOK - copy this from the Deployment Centre in the Azure Portal for your WebApp