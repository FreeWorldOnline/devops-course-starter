FROM python:3.10.8-slim-buster as base

#Install Poetry and project dependencies
RUN pip install "poetry==1.0.5"
COPY poetry.lock poetry.toml pyproject.toml ./
RUN poetry install

#Copy todo_app code
COPY todo_app ./todo_app


#Configure Development
FROM base as development

# Dev specific env vars
ENV FLASK_ENV=development
ENV DEBUG=True
ENV WEBAPP_PORT=5000
EXPOSE ${WEBAPP_PORT}

# Prepare the entry point script
COPY docker_entry_dev.sh docker_entry_dev.sh
RUN ["chmod", "+x", "./docker_entry_dev.sh"]
ENTRYPOINT [ "./docker_entry_dev.sh" ]


#Configure Test
FROM base as test

# Set the working directory as todo_app and entry point to pytest
WORKDIR /todo_app
ENTRYPOINT ["poetry", "run", "pytest"]


#Configure Production
FROM base as production

ENV WEBAPP_PORT=8000
EXPOSE ${WEBAPP_PORT}

# Prepare the entry point script
COPY docker_entry_prod.sh docker_entry_prod.sh
RUN ["chmod", "+x", "./docker_entry_prod.sh"]
ENTRYPOINT [ "./docker_entry_prod.sh" ]